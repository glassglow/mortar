#include <mortar/mortar.h>
#include <mortar/platform.h>

int mortar_init(void) {
  return mortar_platform_init();
}

int mortar_quit(void) {
  return mortar_platform_quit();
}
