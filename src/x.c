#include <mortar/platform/x.h>
#include <mortar/status.h>

#include <X11/Xlib.h>

static Display *mortar_x_display = NULL;

int mortar_x_init(void) {
  if(mortar_x_display != NULL) {
    return MORTAR_STATUS_SUCCESS;
  }

  mortar_x_display = XOpenDisplay(NULL);

  if(mortar_x_display == NULL) {
    return MORTAR_STATUS_XLIB_ERROR;
  } else {
    return MORTAR_STATUS_SUCCESS;
  }
}

int mortar_x_quit(void) {
  if(mortar_x_display == NULL) {
    return MORTAR_STATUS_SUCCESS;
  }

  XCloseDisplay(mortar_x_display);
  return MORTAR_STATUS_SUCCESS;
}
