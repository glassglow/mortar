#ifndef MORTAR_PLATFORM_H
#define MORTAR_PLATFORM_H

#ifdef MORTAR_PLATFORM_X

#include <mortar/platform/x.h>

#define mortar_platform_init mortar_x_init
#define mortar_platform_quit mortar_x_quit

#endif /* MORTAR_PLATFORM_X */

#endif
