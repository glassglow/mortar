#ifndef MORTAR_PLATFORM_X_H
#define MORTAR_PLATFORM_X_H

int mortar_x_init(void);
int mortar_x_quit(void);

#endif
