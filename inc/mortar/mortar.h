#ifndef MORTAR_H
#define MORTAR_H

#include <mortar/export.h>

#ifdef __cplusplus
extern "C" {
#endif

MORTAR_EXPORT int mortar_init(void);
MORTAR_EXPORT int mortar_quit(void);

#ifdef __cplusplus
}
#endif

#endif
